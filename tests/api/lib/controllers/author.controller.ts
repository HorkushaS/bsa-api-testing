import { idText } from 'typescript';
import { ApiRequest } from '../request';

let baseUrl: string = 'https://knewless.tk/api/';

export class AuthorController {
    async getAuthor(accessToken: string) {
        const response = await new ApiRequest()
        .prefixUrl(baseUrl)
            .method('GET')
            .url(`author`)
            .bearerToken(accessToken)
            .send();
        return response;
    };
    async postAuthor(accessToken: string, biography: string, company: string, firstName: string, id: string, job: string, lastName: string, location: string, twitter: string, userId: string, website:string) {
        const response = await new ApiRequest()
        .prefixUrl(baseUrl)
            .method('POST')
            .url(`author`)
            .bearerToken(accessToken)
            .body({
                // "avatar": "string",
                "biography": "string",
                "company": "string",
                "firstName": "string",
                "id": {id},
                "job": "string",
                "lastName": "string",
                "location": "string",
                "twitter": "string",
                "userId": {userId},
                "website": "string"
              })
            .send();
        return response;
    };
    async getUserME(accessToken: string) {
        const response = await new ApiRequest()
        .prefixUrl(baseUrl)
            .method('GET')
            .url(`user/me`)
            .bearerToken(accessToken)
            .send();
        return response;
    };
    async getOverview(accessToken: string, id: string) {
        const response = await new ApiRequest()
        .prefixUrl(baseUrl)
            .method('GET')
            .url(`author/overview/${id}`)
            .bearerToken(accessToken)
            .send();
        return response;
    };
    async postArticle(accessToken: string, id:string, aName:string, image:string, name:string, text:string) {
        const response = await new ApiRequest()
        .prefixUrl(baseUrl)
            .method('POST')
            .url(`article`)
            .bearerToken(accessToken)
            .body({
                "authorId": id,
                "authorName": aName,
                // "id": id,
                "image": "string",
                "name": "string",
                "text": "string"
            })
            .send();
        return response;
    };
    async getArticle(accessToken: string) {
        const response = await new ApiRequest()
        .prefixUrl(baseUrl)
            .method('GET')
            .url(`article/author`)
            .bearerToken(accessToken)
            .send();
        return response;
    };
    async getArticlebyid(accessToken: string, articleId: string) {
        const response = await new ApiRequest()
        .prefixUrl(baseUrl)
            .method('GET')
            .url(`article/author/${articleId}`)
            .bearerToken(accessToken)
            .send();
        return response;
    };
    async postArticleComment(accessToken: string, articleId: string, id: string, text: string) {
        const response = await new ApiRequest()
        .prefixUrl(baseUrl)
            .method('POST')
            .url(`article_comment`)
            .bearerToken(accessToken)
            .body({
                "articleId": articleId,
                "authorId": id,
                "text": "string"
            })
            .send;
        return response;
    };
    async getArticleComment(accessToken: string, articleId: string) {
        const response = await new ApiRequest()
        .prefixUrl(baseUrl)
            .method('GET')
            .url(`/article_comment/of/${articleId}?size=200`)
            .bearerToken(accessToken)
            .send;
        return response;
    };
}