import { expect } from "chai";
import { checkStatusCode } from '../../helpers/functionsForChecking.helper';
import { AuthController } from "../lib/controllers/auth.controller";
import { AuthorController } from "../lib/controllers/author.controller";
const auth = new AuthController();
const author = new AuthorController();
const schemas = require('./data/schemas_testData.json');
var chai = require('chai');
chai.use(require('chai-json-schema'));

describe("auth controller", () => {
    let accessToken: string; let id: string; let userId: string; let aName: string; let articleId: string; let sourceId: string
    
    it(`login`, async () => {
        let response = await auth.authenticateUser('bsa22@mailsac.com', '123456Aa');
        checkStatusCode(response, 200);
        accessToken = response.body.accessToken;
    });
    
    it(`author-check`, async () => {
        let response = await author.getAuthor(accessToken);
        checkStatusCode(response, 200);
        console.log(response.body);
        id = response.body.Id;
        userId = response.body.userId;
        aName = response.body.firstName;
    });
    it(`author-change`, async () => {
        let response = await author.postAuthor(accessToken, "MEandME", "Binary Studio", "Stanislav", id, "QA", "Horkusha", "Kyiv", "twitter.com", userId, "mywebsite.com");
        checkStatusCode(response, 200);
        console.log(response.body);
    });
    it(`my-info-check`, async () => {
        let response = await author.getUserME(accessToken);
        checkStatusCode(response, 200);
        console.log(response.body);
    });
    it(`author-overview`, async () => {
        let response = await author.getOverview(accessToken, id);
        checkStatusCode(response, 200);
        console.log(response.body);
    });
    it(`post-article`, async () => {
        let response = await author.postArticle(accessToken, id, aName, "someimage.png", "Article", "my Article");
        checkStatusCode(response, 200);
        console.log(response.body);
        articleId = response.body.id;
    });
    it(`get-article`, async () => {
        let response = await author.getArticle(accessToken);
        checkStatusCode(response, 200);
        console.log(response.body);
    });
    it(`get-article-by-id`, async () => {
        let response = await author.getArticlebyid(accessToken, articleId);
        checkStatusCode(response, 200);
        console.log(response.body);
    });
    // it(`post-article-comment`, async () => {
    //     let response = await author.postArticleComment(accessToken, articleId, id,  "This is my test comment!");
    //     checkStatusCode(response, 200);
    //     console.log(response.body);
    //     sourceId = response.body.sourceId;
    // });
    // it(`get-article-comment`, async () => {
    //     let response = await author.getArticleComment(accessToken, articleId);
    //     checkStatusCode(response, 200);
    //     console.log(response.body);
    // });
});
